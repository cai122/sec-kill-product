package com.lanchong.utils;

import lombok.experimental.UtilityClass;

import java.util.UUID;

/**
 * 唯一id生成
 */
@UtilityClass
public class UUIDUtil {
	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
